//////////////////////////////////////////////////////////////////////////////////
// Example of export from the DCL Lighting blender demo
// Blender file by sleekdigital#8225
// Permission given by sleekdigital#8225 to use this with appropriate credits.
////////////////////////////////////////////////////////////////////////////////// 


let lightingDemo = new Entity()

// let lightingDemoShape = new BoxShape()
let lightingDemoShape = new GLTFShape("models/LightingDemoCurved.glb")
lightingDemo.addComponent(lightingDemoShape)

let lightingDemoTransform = new Transform({position:new Vector3(0,0,0)})
lightingDemo.addComponent(lightingDemoTransform)

engine.addEntity(lightingDemo)

